package Exceptions;

import javax.swing.*;

public class InvalidInputException extends Exception
{
    public InvalidInputException()
    {
        super(JOptionPane.showInputDialog("Invalid Input."));
    }
}
