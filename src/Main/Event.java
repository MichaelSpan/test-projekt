package Main;

import Exceptions.InvalidInputException;

import javax.swing.*;

public class Event
{
    private RNG rng = new RNG();
    private Battle battle = new Battle();
    private boolean kisteOffen = false;
    private String input;
    private Enemy goblin = new Enemy("Goblin",30,30, 15, 10, 5);
    private Enemy sklavenHaendler = new Enemy("Sklaven Haendler",70, 70, 25, 10, 5);
    private Enemy magier = new Enemy("Magier", 50, 50, 35, 5, 20);
    private Enemy skelettKrieger = new Enemy("Skelett Krieger", 50,50, 30, 15, 0);
    private Enemy daemon = new Enemy("Daemon", 50, 50, 40, 20, 0);

    public Event()
    {
    }

    public void randomEvent(Player player) throws InvalidInputException
    {
        int event = rng.eventCheck(15);
        switch(event)
        {
            case 1://Höhle mit fallenden Felsen
                input= JOptionPane.showInputDialog("Du durchquerst ein gefaehrliches Höhlensystem um auf die andere Seite eines Berges zu kommen.\nPlötzlich beginnt der Boden zu vibrieren und der Gang scheint Einzustürzen! Springst du nach:\n1 Links?\n2 Rechts?");
                if(input.equals("1")||input.equals("2"))
                {
                    if(rng.coinFlipWin() == true)
                    {
                        JOptionPane.showMessageDialog(null, "Ein grosser Felsen verfehlt dich nur knapp und du kommst ohne weitere\nVerletzungen aus dem Berg wieder heraus.");
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Ein grosser Felsen streift dich als er von der Decke stuerzt.\n-10 HP");
                        player.takeDamageNoDef(10);
                        if(player.getHealth()>0)
                        {
                            JOptionPane.showMessageDialog(null, "Du stehst nach dem harten Treffer schnell auf und rettest dich aus der Hoele.");
                        }
                    }
                }else if (input.equals(3))
                {
                    JOptionPane.showMessageDialog(null, "Du konntest dich nicht entscheiden und wirst von einem Felsen getroffen.\nDu kommst nur unter groesster Muehe aus der Höhle.\n-20 HP");
                    player.takeDamageNoDef(20);
                } else
                {
                    throw new InvalidInputException();
                }
                break;
            case 2://Bruecke
                input=JOptionPane.showInputDialog("Du findest eine Bruecke welche nicht gerade sehr stabil aussieht, das ueberqueren\nwürde dir jedoch zeit geben ein wenig zu rasten bevor du weiterziehst.\n1 Bruecke ueberqueren\n2 Bruecke nicht ueberqueren");
                if(input.equals("1"))
                {
                    if(rng.coinFlipWin() == true)
                    {
                        JOptionPane.showMessageDialog(null, "Du machst deine ersten Schritte auf der Bruecke und sie scheint zu halten!\nDu schaffst es die Bruecke zu ueberqueren und gönnst dir eine verdiente Rast.\n+5 HP");
                        player.heal(5);
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Deine ersten Schritte gehen gut, doch die Bruecke beginnt unter deinen Füßen\nwegzubrechen! Faellst zu Glueck nicht sehr weit, doch du bist verletzt.\nnun musst du einen Umweg machen um aus dem Graben heraus zu kommen.\n-15 HP");
                        player.takeDamageNoDef(15);
                    }
                }else if(input.equals("2"))
                {
                    JOptionPane.showMessageDialog(null, "Du entscheidest dich weiter zu gehen.");
                }else
                {
                    throw new InvalidInputException();
                }
                break;
            case 3://Reisender Heiler
                JOptionPane.showMessageDialog(null, "Du triffst auf der Strasse einen reisenden der dir anbietet dich, fuer eine kleine Summe\nGold, zu Heilen.\nDu hast "+ player.getGold()+" Gold und "+ player.getHealth()+" HP.");
                if(player.getGold()>=30)
                {
                    input=JOptionPane.showInputDialog("Möchtest du Das Angebot des Reisenden annehmen?\n1 Ja (-30 Gold, +10 HP)\n2 Nein");
                    if(input.equals("1"))
                    {
                        player.transaction(-30);
                        player.heal(10);
                    } else if (input.equals("2"))
                    {
                        JOptionPane.showMessageDialog(null, "Du lehnst ab und ziehst weiter.");
                    } else
                    {
                        throw new InvalidInputException();
                    }
                }else
                {
                    JOptionPane.showMessageDialog(null, "Du hast leider nicht genug Gold um dir den Dienst des Reisenden zu leisten.\nJener hat Mitleid mit dir und heilt dich dennoch ein wenig.");
                    player.heal(5);
                }
                break;
            case 4://Goblin angriff
                JOptionPane.showMessageDialog(null,"Eine kleine Gruppe Goblins lauert dir auf und attackiert dich!");
                battle.fight(player, goblin);
                if(player.getHealth()>0)
                {
                    JOptionPane.showMessageDialog(null,"Du besiegst die Goblins und durchsuchst sie nach Wertgegenständen.\n+ 10 Gold");
                    player.transaction(10);
                }
                break;
            case 5://Drachenangriff
                JOptionPane.showMessageDialog(null, "Du wirst das Opfer eines Drachenangriffs. Der Drache griff eine Karawane\nin der Naehe an und du wurdest von den Flammen erfasst.\n-30 HP");
                player.takeDamageNoDef(30);
                break;
            case 6://Labyrith
                JOptionPane.showMessageDialog(null, "Vor dir erschliesst sich ein grosses magisches Labyrith.\nDu entscheidest dich hineinzugehen.");
                input=JOptionPane.showInputDialog("Gib ein in welcher reihenfolge du im Labyrinth abbigen möchtest:\nHINWEIS die abfolge besteht aus 4 Zahlen(Beispiel: 3221).\n1 Links\n2 Rechts\n3 Gerade aus");
                if(input.equals("1132"))
                {
                    if(kisteOffen==false)
                    {
                        JOptionPane.showMessageDialog(null, "Du findest im innersten eine Kiste gefüllt mit Gold!! +100 Gold");
                        player.transaction(100);
                        kisteOffen = true;
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Du findest dich im innersten des Labyriths wieder, die leere Kiste neben dir.\nDu nutzt den stillen Ort um dich kurz auszuruhen.\n+10 HP");
                        player.heal(10);
                    }
                }else
                {
                    JOptionPane.showMessageDialog(null, "Du findest dich nach langem umherirren wieder am Eingang\ndes Labyrinths und beschliesst weiter zu reisen.");
                }
                break;
            case 7://Stadt
                JOptionPane.showMessageDialog(null, "Du kommst zu einer kleinen Stadt, hier gibt es ein Gasthaus in dem du Rasten könntest.");
                if(player.getGold()>=30)
                {
                    input=JOptionPane.showInputDialog("Möchtest du hier rasten?\n1 Ja (-30 Gold, +20 HP)\n2 Nein");
                    if(input.equals("1"))
                    {
                        player.transaction(-30);
                        player.heal(20);
                    }
                }else
                {
                    JOptionPane.showMessageDialog(null, "Du hast leider nicht genug Gold um dir ein Zimmer zu leisten.\nDu ziehst weiter.");
                }
                break;
            case 8://Sklavenhaendler
                input=JOptionPane.showInputDialog("Du triffst auf eine Gruppe Sklavenhaendler mit 2 Sklaven im Schlepptau.\nWillst du versuchen diese zu befreien?\n1 Ja\n2 Nein");
                if(input.equals("1"))
                {
                    JOptionPane.showMessageDialog(null,"Die Sklavenhaendler haben offensichtlich entwas gegen deine Entscheidung ihre Sklaven zu befreien.\n Sie greifen an!");
                    battle.fight(player, sklavenHaendler);
                }else if(input.equals("2"))
                {
                    JOptionPane.showMessageDialog(null, "Es waere definitiv keine kluge Entscheidung sich mit diesen Leuten anzulegen.\nWer weiss, vielleicht würdest du sogar in Ketten enden und selbst Sklave werden.");
                }else
                {
                    JOptionPane.showMessageDialog(null, "Da du dich nicht entscheiden konntest zog die Gruppe stillschweigend vorbei.\nVielleicht war es auch besser so...");
                }
                break;
            case 9://Verrückter Magier
                input=JOptionPane.showInputDialog("Ein Fremder stoppt dich auf der Durchreise in einem Dorf.\nEr ist ein alter Mann mit, ganz klar erkentlich, magischen Roben und einem Spitzen Hut.\nDer Mann schreit mehr als er spricht und fordert dich zum kampf auf. Er ist offensichtlich nicht bei Sinnen.\n1 Herausforderung annehmen\n2 Herausforderung ablehnen");
                if(input.equals("1"))
                {
                    JOptionPane.showMessageDialog(null, "Du stellst dich dem Magier.");
                    battle.fight(player, magier);
                    if(player.getHealth()>0)
                    {
                        JOptionPane.showMessageDialog(null,"Du stehst nach dem harten Kampf schnell auf und erledigst den Magier\nmit nur einem wuchtigen Schlag. Ein Paar Leute applaudieren dir.");
                    }
                }else if(input.equals("2"))
                {
                    JOptionPane.showMessageDialog(null, "Der Magier schreit wie ein Besessener als du dich ihm abwendest,\nEr trifft dich mit einem Flammenzauber in den Rücken und du flüchtest.\n-20 HP");
                    player.takeDamageNoDef(20);
                }else
                {
                    JOptionPane.showMessageDialog(null, "Du bleibst still und sagst nichts. Der Magier scheint sich dadurch zu beruhigen.\nEr klopft dir auf die Schulter und lacht laut als er einfach verschwindet.");
                }
                break;
            case 10://Skelettkrieger
                JOptionPane.showMessageDialog(null,"Ein Skelettkriger überrascht dich als du Nachts im Wald schläfst.\nEr liefert dir einen harten Kampf.");
                battle.fight(player, skelettKrieger);
                break;
            case 11://Einhorn
                input=JOptionPane.showInputDialog("Als du eine sehr dunkle Hoele durchqueerst entdeckst du ein licht\nin der Entfernung. Willst du es dir genauer ansehen?\n1 Ja auf jeden fall!\n2 Nein. Ich habe kein gutes Gefuehl bei der Sache.");
                if(input.equals("1"))
                {
                    input="0";
                    input=JOptionPane.showInputDialog("Als du dich dem Leuchten naeherst erkennst du was es verursacht.\nEs ist ein Einhorn, diese Kreaturen gelten als ausgestorben, doch\nhier vor dir ist ein echtes, ein lebendiges. Das Horn zu beruehren soll glueck bringen.\nWirst du:\n1Dich nähern um das Horn zu berühren\n2 Die arme Kreatur in Ruhe lassen");
                    if(input.equals("1"))
                    {
                        if(rng.coinFlipWin() == true)
                        {
                            JOptionPane.showMessageDialog(null, "Du naeherst dich dem Wesen vorsichtig und berührst schlussendlich das Horn.\nDas Einhorn scheint ruhig zu bleiben.\n+30 HP");
                            player.heal(30);
                        }else
                        {
                            JOptionPane.showMessageDialog(null, "Das Einhorn scheint sich durch deine Annaeherung bedraengt zu fuehlen.\nDir wird schwindlig und Schlussendlich erwachst du voller Schmerz\nwieder am Eingang der Hoehle.");
                            if(player.getGold()>100)
                            {
                                JOptionPane.showMessageDialog(null, "Bald darauf such dich jedoch ein Unglueck heim.\n-30 HP -100 Gold");
                                player.takeDamageNoDef(30);
                                player.transaction(-100);
                            }else
                            {
                                JOptionPane.showMessageDialog(null, "Bald darauf such dich jedoch ein Unglueck heim.\n-30 HP -All dein Gold");
                                player.takeDamageNoDef(30);
                                player.transaction(-100);
                            }
                        }
                    }else if(input.equals("2"))
                    {
                        JOptionPane.showMessageDialog(null, "Es ist wohl besser das Einhorn in Ruhe zu lassen. Es ist eines der Letzten\nseiner Art und wahrschinlich versteckt es sich hier vor Menschen die\nes für sein Glueckshorn jagen.\nAls du die Hoele verlässt spürst du jedoch wie dich etwas beruehrt.\nDas Einhorn steht neben dir und gab dir einen Teil seines Gluecks.\n+15 HP");
                        player.heal(15);
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Du kannst dich nicht entscheiden was du tun sollst, das Wesen zieht dich mit seiner\nmagischen Aura in seinen Bann. Du vergisst einen ganzen Tag lang zu essen\nund zu trinken. Du fuehlst dich schwach als du zu dir kommst.\n-15 HP");
                        player.takeDamageNoDef(15);
                    }
                }else
                {
                    JOptionPane.showMessageDialog(null, "Dir ist nicht klar was vor dir ligt und das Risiko kann es nicht wert sein.\nDu ziehst weiter.");
                }
                break;
            case 12://Magische Forscher
                input=JOptionPane.showInputDialog("Du triffst auf eine Gruppe Elfen die an einer Bergwand Ausgrabungen machen.\nEiner von ihnen kommt auf dich zu und bittet dich umzukehren, da sie ein uraltes Artefakt\nfreigesetzt haben, welches nun die Seelen unwissender Reisender sucht.\n1 Ignorier die Warnung, du musst weiter.\n2 Kehr um, du kannst nichts riskieren.");
                if(input.equals("1"))
                {
                    if(rng.coinFlipWin()==true)
                    {
                        JOptionPane.showMessageDialog(null, "Als du weiter gehst spuerst du wie dich etwas beobachtet, ein Daemon attakiert dich!");
                        battle.fight(player, daemon);
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Du kommst sicher an dein Ziel und du findest unterwegs einen fallengelassenen Geldbeutel.\n+30 Gold");
                        player.transaction(30);
                    }
                }else
                {
                    JOptionPane.showMessageDialog(null,"Es ist definitiv keine gute Idee weiter zu gehen.\nDu kehrst um und nimmst einen Umweg.");
                }
                break;
            case 13://"Haendler"
                JOptionPane.showMessageDialog(null,"Du triffst Haendler die vor Sonnenuntergang an der Strasse ein Lager aufgeschlagen haben.\nDu entscheidest dich dazu mit ihnen zu trinken und dort zu schlafen.\nDie Haendler sind jedoch verkleidete Banditen gewesen die nur auf jemanden\nwie dich gewartet haben.");
                if(player.getGold()>150)
                {
                    JOptionPane.showMessageDialog(null,"Sie nehmen dir im schlaf deine Wertgegenstände ab und du wachst alleine auf.\n-150 Gold.");
                    player.transaction(-150);
                }else
                {
                    JOptionPane.showMessageDialog(null,"Sie nehmen dir im schlaf deine Wertgegenstände ab und du wachst alleine auf.\nDu verlierst dein ganzes Gold.");
                    player.transaction(-player.getGold());
                }
                break;
            case 14://Sirenen See
                input=JOptionPane.showInputDialog("Ein wunderschoener See breitet sich vor dir aus. Es ist der See der Sirenen vor dem dich\nandere reisende bereits gewarnt haben. Doch sind das nicht nur Maerchen?\n1 Naehere dich dem See, das sind nur Geschichten.\n2 Es muss etwas an den Warnungen der Anderen drann sein. Bleib fern.");
                if(input.equals("1"))
                {
                    JOptionPane.showMessageDialog(null, "Eine Melodie ertoent die wie der schoenste Gesang klingt den du je gehoert hast.\nDu bist im Bann der Sirenen des Sees...\n-80 HP");
                    player.takeDamageNoDef(80);
                }else if(input.equals("2"))
                {
                    JOptionPane.showMessageDialog(null,"Du suchst das Weite. Dein Leben ist dir wichtiger als alte Geschichten auf die Probe zu stellen.");
                }else
                {
                    JOptionPane.showMessageDialog(null, "Du stehst unentschlossen herum, als ploetzich\neine Melodie ertoent die wie der schoenste Gesang klingt den du je gehoert hast.\nDu bist bereits im Bann der Sirenen des Sees...\n-80 HP");
                    player.takeDamageNoDef(80);
                }
                break;
            case 15://Eventlos oder Deal mit einem Daemon
                if(player.getHealth()>10)
                {
                    JOptionPane.showMessageDialog(null, "Es ist ein normaler Tag auf deinen Reisen und nichts besonderes passiert.");
                }else
                {
                    input=JOptionPane.showInputDialog("Vor dir erscheint ein Daemon der deine Schwaeche riecht. Er erklärt dir, dass\ndu so nicht mehr lange überlebst. Er bietet dir an dich zu heilen.\n1 Dem Daemon vertrauen\n2 Vertraue niemals einem Daemon");
                    if(input.equals("1"))
                    {
                        JOptionPane.showMessageDialog(null, "Der Daemon lacht und ersticht dich mit seinem Schattenschwert.\n-10 HP");
                    }else if(input.equals("2"))
                    {
                        JOptionPane.showMessageDialog(null, "Der Daemon wird wuetend, doch kann ohne deine Zustimming nichts machen. Er verschwindet.");
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "Du tust so als ob du den Daemon nicht siehst und ignorierst ihn komplett.\nDies verwirrt ihn, doch scheint zu funktionieren. Er verschwindet.");
                        JOptionPane.showMessageDialog(null, "Die Goetter sind mit deiner Standhaftigkeit beeindruckt und Heilen dich.\n+50 HP");
                        player.heal(50);
                    }
                }
                break;
        }
    }
}