package Main;

public class Character
{
    private String name;
    private int health;
    private int maxHealth;
    private int attack;
    private int defense;
    private int critRate;

    public Character(String name, int maxHealth, int health, int attack, int defense, int critRate)
    {
        this.name = name;
        this.maxHealth = maxHealth;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.critRate = critRate;
    }

    public Character(String name)
    {
        this.name = name;
    }

    public void takeDamageNoDef(int damage)
    {
        setHealth(getHealth() - damage);
    }

    public void takeDamage(int damage)
    {
        if (this.defense > damage)
        {
            damage = 0;
        }
        setHealth((getHealth() + defense) - damage);
        this.health -= defense;
    }

    public void attack(Character enemy)
    {
        enemy.takeDamage(this.getAttack());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth()
    {
        return health;
    }

    public void setHealth(int health)
    {
        if (this.health < 0)
        {
            this.health = 0;
        }
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void heal(int amount)
    {
        if ((getHealth() + amount) > this.maxHealth)
        {
            setHealth(this.maxHealth);
        }else
        {
            setHealth(getHealth() + amount);
        }
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getCritRate() {
        return critRate;
    }

    public void setCritRate(int critRate) {
        this.critRate = critRate;
    }

    @Override
    public String toString() {
        return  "name='" + name + '\'' +
                ", health=" + health +
                ", attack=" + attack +
                ", defense=" + defense +
                ", critRate=" + critRate;
    }
}
