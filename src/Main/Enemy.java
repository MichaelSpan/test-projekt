package Main;

public class Enemy extends Character
{
    public Enemy(String name, int maxHealth, int health, int attack, int defense, int critRate)
    {
        super(name, maxHealth, health, attack, defense, critRate);
    }
}
