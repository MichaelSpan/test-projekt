package Main;

import javax.swing.*;
import java.util.Random;

public class RNG
{
    private Random random = new Random();

    public boolean coinFlipWin()
    {
        int result = random.nextInt(2);
        if (result == 1)
        {
            JOptionPane.showMessageDialog(null, "true");
            return true;
        } else
        {
            JOptionPane.showMessageDialog(null, "false");
            return false;
        }
    }

    public boolean percentageCheck(int percentage)
    {
        if (random.nextInt(100)+1 <= percentage)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public int eventCheck(int numberOfEvents)
    {
        int result = random.nextInt(numberOfEvents)+1;
        return result;
    }
}
