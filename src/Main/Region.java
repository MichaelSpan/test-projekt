package Main;

import Exceptions.InvalidInputException;

import java.util.HashMap;
import java.util.Set;

public class Region {
    private final HashMap<String, Region> paths;
    private static boolean eventPlayed;

    public Region()
    {
        paths = new HashMap<>();
        eventPlayed = false;
    }

    public static void playEvent(Player p) throws InvalidInputException
    {
        Event e = new Event();
        e.randomEvent(p);
    }

    public Region getPath(String direction)
    {
        return paths.get(direction);
    }

    public HashMap<String, Region> getPaths() {
        return paths;
    }

    public void setPaths(String direction, Region nextRegion) {
        paths.put(direction, nextRegion);
    }

    public String showPaths()
    {
        String result = " Wege: ";
        Set<String> keys = paths.keySet();
        for (String path : keys)
        {
            result += " " + path;
        }
        return result;
    }
}