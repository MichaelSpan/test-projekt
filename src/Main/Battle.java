package Main;

import Exceptions.InvalidInputException;

import javax.swing.*;

public class Battle
{
    private String battleInput;

    public Battle()
    {
    }

    public void fight(Player player, Enemy enemy) throws InvalidInputException
    {
        while (enemy.getHealth() > 0 && player.getHealth() > 0)
        {
            JOptionPane.showMessageDialog(null, player.getName() + ": " + player.getHealth() + " HP\n" + enemy.getName() + ": " + enemy.getHealth() + " HP");
            setBattleInput(JOptionPane.showInputDialog("Was wird " + player.getName() + " machen?\n1 Angreifen\n2 Blocken (50% kein Schaden)"));
            switch (getBattleInput())
            {
                case "1":
                    player.attack(enemy);
                    JOptionPane.showMessageDialog(null,player.getName() + " greift an!");
                    break;
                case "2":
                    player.block();
                    JOptionPane.showMessageDialog(null,player.getName() + " blockt!");
                    break;
                default:
                    throw new InvalidInputException();
            }
            JOptionPane.showMessageDialog(null, player.getName() + ": " + player.getHealth() + " HP\n" + enemy.getName() + ": " + enemy.getHealth() + " HP");
            if (enemy.getHealth() > 0 && player.getHealth() > 0) {
                JOptionPane.showMessageDialog(null,enemy.getName() + " greift an!");
                enemy.attack(player);
            } else
            {
                if (player.getHealth() > enemy.getHealth()) {
                    JOptionPane.showMessageDialog(null,player.getName() + " gewinnt!");
                } else
                {
                    JOptionPane.showMessageDialog(null,enemy.getName() + " gewinnt!");
                }
                break;
            }
        }
    }

    public String getBattleInput()
    {
        return battleInput;
    }

    public void setBattleInput(String battleInput)
    {
        this.battleInput = battleInput;
    }
}
