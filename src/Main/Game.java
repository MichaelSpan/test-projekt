package Main;

import Exceptions.InvalidInputException;

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Game implements Serializable {
    private Region currentRegion;
    private Region winRegion;
    private boolean win = false;
    private int turnCounter = 0;
    private Player p;
    private String direction;

    public Game() {
        generateWorld();
    }

    public void generateWorld() {
        Region r1 = new Region();
        Region r2 = new Region();
        Region r3 = new Region();
        Region r4 = new Region();
        Region r5 = new Region();
        Region r6 = new Region();
        Region r7 = new Region();
        Region r8 = new Region();
        Region r9 = new Region();
        Region r10 = new Region();
        Region r11 = new Region();
        Region r12 = new Region();
        Region r13 = new Region();
        Region r14 = new Region();
        Region r15 = new Region();
        Region s1 = new Region();
        Region s2 = new Region();
        Region s3 = new Region();
        Region s4 = new Region();
        Region s5 = new Region();
        Region s6 = new Region();
        Region s7 = new Region();

        r1.setPaths("oben", r2);
        r2.setPaths("links", s1);
        s1.setPaths("rechts", r2);
        r2.setPaths("unten", r1);
        r2.setPaths("rechts", r3);
        r3.setPaths("links", r2);
        r3.setPaths("rechts", s2);
        s2.setPaths("links", r3);
        r3.setPaths("oben", r4);
        r4.setPaths("unten", r3);
        r4.setPaths("oben", r5);
        r5.setPaths("unten", r4);
        r5.setPaths("rechts", s3);
        s3.setPaths("links", r5);
        s3.setPaths("rechts", s4);
        s4.setPaths("links", s3);
        r5.setPaths("links", r6);
        r6.setPaths("rechts", r5);
        r6.setPaths("oben", r7);
        r7.setPaths("unten", r6);
        r7.setPaths("links", r8);
        r8.setPaths("rechts", r7);
        r8.setPaths("oben", s5);
        s5.setPaths("unten", r8);
        s5.setPaths("oben", s6);
        s6.setPaths("unten", s5);
        r8.setPaths("links", r9);
        r9.setPaths("rechts", r8);
        r9.setPaths("links", r10);
        r10.setPaths("rechts", r9);
        r10.setPaths("unten", r11);
        r11.setPaths("oben", r10);
        r11.setPaths("links", r12);
        r12.setPaths("rechts", r11);
        r12.setPaths("unten", s7);
        s7.setPaths("oben", r12);
        r12.setPaths("links", r13);
        r13.setPaths("rechts", r12);
        r13.setPaths("oben", r14);
        r14.setPaths("unten", r13);
        r14.setPaths("oben", r15);
        r15.setPaths("unten", r14);

        currentRegion = r1;
        winRegion = r15;
    }

    public void game() throws InvalidInputException {
        String name = (JOptionPane.showInputDialog(null, "Wilkommen in Almora, wie ist dein Name?\n(Wenn kein Name eingegeben wird, sucht das Spiel aus einer Liste vorgegebener Namen aus.)\nBitte geben Sie Ihren Namen ein:"));
        p = new Player(name, 100, 100, 20, 10, 10, 30);
        if (p.getName().equals("") || p.getName() == "null") {
            Random selectName = new Random();
            ArrayList<String> randName = new ArrayList();
            randName.add("Michael");
            randName.add("Dominik");
            randName.add("Stefan");
            randName.add("Florian");
            randName.add("Helmut");
            randName.add("Anna");
            randName.add("Lisa");
            randName.add("Sarah");
            randName.add("Christina");
            randName.add("Adrian");
            int selection = selectName.nextInt(randName.size());
            String newName = randName.get(selection);
            p.setName(newName);
        }

        JOptionPane.showMessageDialog(null, p.getName() + " es erwartet dich ein harter Kampf der niemals endend scheint. Du bist nun Teil dieses Kampfes.\nWirst du ueberleben?");
        JOptionPane.showMessageDialog(null, "Dein Ziel ist es Den Heiligen Graal zu finden und damit zum unbesigbaren Helden zu werden.\nDer Graal ist irgendwo in Almora zu finden, doch wo, musst du selbst herausfinden!");
        JOptionPane.showMessageDialog(null, "INFORMATION: 'exit' beendet das Spiel in der Richtungswahl. Das Spiel speichert nicht, das heisst,\ndass das beenden des Spiels zum verlust deines Vortschrittes führt.\nHP sind deine Health Points(Lebenspunkte) und Gold ist die Währung von Almora. Manche Events haben geheime Ausgaenge die man erreicht in dem man\netwas Anderes als gefordert eingibt. Diese Ausgaenge sind meist schlechter als die vorgegebenen, doch\nmanchmal lohnt es sich das Unbekannte herauszufordern!");
        JOptionPane.showMessageDialog(null, "Nun gut. Viel Erfolg Abenteurer. Mögen euch die Götter beschützen!");

        boolean gameOver = false;

        while (!gameOver) {
            JOptionPane.showMessageDialog(null, "Du hast aktuell " + p.getHealth() + " HP und " + p.getGold() + " Gold.");
            if (p.getHealth() <= 0) {
                JOptionPane.showMessageDialog(null,"GAME OVER! Du hast das zeitliche gesegnet.");
                JOptionPane.showMessageDialog(null, "Du hast " + turnCounter + " Regionen durchqueert, doch den Graal leider nicht gefunden...");
                gameOver = true;
                break;
            } else if (currentRegion == winRegion) {
                JOptionPane.showMessageDialog(null,"Glückwunsch! Du hast den Graal gefunden und Gewonnen!");
                JOptionPane.showMessageDialog(null, "Du hast " + turnCounter + " Regionen durchqueert!");
                this.win = true;
                gameOver = true;
                break;
            }

            Region.playEvent(p);

            if (p.getHealth() <= 0) {
                JOptionPane.showMessageDialog(null,"GAME OVER! Du hast das zeitliche gesegnet.");
                JOptionPane.showMessageDialog(null, "Du hast " + turnCounter + " Regionen durchqueert, doch den Graal leider nicht gefunden...");
                gameOver = true;
                break;
            } else if (currentRegion == winRegion) {
                JOptionPane.showMessageDialog(null,"Glückwunsch! Du hast den Graal gefunden und Gewonnen!");
                JOptionPane.showMessageDialog(null, "Du hast " + turnCounter + " Regionen durchqueert!");
                this.win = true;
                gameOver = true;
                break;
            }

            direction = JOptionPane.showInputDialog("Wohin solls gehen?" + currentRegion.showPaths());
            turnCounter++;
            Region nextRegion = currentRegion.getPath(direction);
            if (nextRegion == null) {
                JOptionPane.showMessageDialog(null, "In diese Richtung führt kein Pfad.");
            } else {
                currentRegion = nextRegion;
            }
        }
    }
}