package Main;

public class Player extends Character {
    private int gold;
    private RNG rng = new RNG();

    public Player(String name, int maxHealth, int health, int attack, int defense, int critRate, int gold)
    {
        super(name, maxHealth, health, attack, defense, critRate);
        this.gold = gold;
    }

    public boolean block()
    {
        if (rng.coinFlipWin() == true)
        {
            return true;
        } else
        {
            return false;
        }
    }

    @Override
    public void takeDamage(int damage) {
        if (block() == true)
        {
            damage = 0;
        } else
        {
            super.takeDamage(damage);
        }
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold)
    {
        this.gold = gold;
    }

    public void transaction(int amount)
    {
        if (getGold() + amount < 0)
        {
            setGold(0);
        }
        setGold(getGold() + amount);
    }

    @Override
    public String toString()
    {
        return "Player{" + super.toString() +
                ", gold=" + gold +
                '}';
    }
}